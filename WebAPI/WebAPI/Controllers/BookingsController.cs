﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using WebAPI.Models;

namespace WebAPI.Controllers

{
    [ApiController]
    [Route("api/[controller]")]

    public class BookingsController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        public BookingsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        //API GET ALL BOOKINGS REQUEST
        [HttpGet]
        public JsonResult Get()
        {

            //SQL QUERY
            string query = @"
                    select BookingId, UserName, Room, TimeFrom, TimeTo from dbo.Bookings ORDER BY TimeFrom ASC";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }



        //API INSERT REQUEST
        [HttpPost]
        public JsonResult Post(Bookings booking)
        {

            //SQL QUERY
            string query = @"
                    insert into dbo.Bookings 
                    (UserName,Room,TimeFrom,TimeTo)
                    values 
                    (
                    '" + booking.UserName + @"'
                    ,'" + booking.Room + @"'
                    ,'" + booking.TimeFrom.ToString("yyyy/MM/dd HH:mm:ss") + @"'
                    ,'" + booking.TimeTo.ToString("MM/dd/yyyy HH:mm:ss") + @"'
                    )
                    ";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Added Successfully");
        }



        //API UPDATE REQUEST
        [HttpPut]
        public JsonResult Put(Bookings booking)
        {
            //SQL QUERY
            string query = @"
                    update dbo.Bookings set 
                    UserName = '" + booking.UserName + @"'
                    ,Room = '" + booking.Room + @"'
                    ,TimeTo = '" + booking.TimeTo + @"'
                    ,TimeFrom = '" + booking.TimeFrom + @"'
                    where BookingId = " + booking.BookingId + @" 
                    ";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Updated Successfully");
        }


        //API DELETE REQUEST
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {

            //SQL QUERY
            string query = @"
                    delete from dbo.Bookings
                    where BookingId = " + id + @" 
                    ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Deleted Successfully");
        }


        //API TO GET ALL BOOKINGS FROM THE BOARDROOM
        [Route("GetTheBoardroom")]
        public JsonResult GetTheBoardroom()
        {

            //SQL QUERY
            string query = @"
                    select BookingId, UserName, Room, TimeFrom, TimeTo from dbo.Bookings where Room = " + "'The Boardroom'" + "ORDER BY TimeFrom ASC" + @" 
                    ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //API TO GET ALL BOOKINGS FOR THE FISHBOWL
        [Route("GetTheFishbowl")]
        public JsonResult GetTheFishbowl()
        {

            //SQL QUERY
            string query = @"
                    select BookingId, UserName, Room, TimeFrom, TimeTo from dbo.Bookings where Room = " + "'The Fishbowl'" + "ORDER BY TimeFrom ASC" + @" 
                    ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }


        //API TO GET ALL BOOKED TIMES FROM THE BOARDROOM
        [Route("GetTheBoardroomTimes")]
        public JsonResult GetTheBoardroomTimes()
        {

            //SQL QUERY
            string query = @"
                    select TimeFrom, TimeTo from dbo.Bookings where Room = " + "'The Boardroom'" + "ORDER BY TimeFrom ASC" + @" 
                    ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        //API TO GET ALL BOOKED TIMES FOR THE FISHBOWL
        [Route("GetTheFishbowlTimes")]
        public JsonResult GetTheFishbowlTimes()
        {

            //SQL QUERY
            string query = @"
                    select TimeFrom, TimeTo from dbo.Bookings where Room = " + "'The Fishbowl'" + "ORDER BY TimeFrom ASC" + @" 
                    ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        [Route("GetTheFishbowlTimesFromDate/{date}")]
        public JsonResult GetTheFishbowlTimesFromDate(DateTime date)
        {
            string stringdate = date.ToString();

            string year = stringdate.Substring(6, 4);
            string month = stringdate.Substring(3, 2);
            string day = stringdate.Substring(0, 2);


            //SQL QUERY
            string query = @"
                    select BookingId, UserName, Room, TimeFrom, TimeTo from dbo.Bookings where Room = 'The Fishbowl' 
                    AND (DATEPART(yy, TimeFrom) = " + year + "AND DATEPART(mm, TimeFrom) = " + month + " AND DATEPART(dd, TimeFrom) = " + day + ") ORDER BY TimeFrom ASC" + @" ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        [Route("GetTheBoardroomTimesFromDate/{date}")]
        public JsonResult GetTheBoardRoomTimesFromDate(DateTime date)
        {
            string stringdate = date.ToString();

            string year = stringdate.Substring(6, 4);
            string month = stringdate.Substring(3, 2);
            string day = stringdate.Substring(0, 2);


            //SQL QUERY
            string query = @"
                    select BookingId, UserName, Room, TimeFrom, TimeTo from dbo.Bookings where Room = 'The Boardroom' 
                    AND (DATEPART(yy, TimeFrom) = " + year + "AND DATEPART(mm, TimeFrom) = " + month + " AND DATEPART(dd, TimeFrom) = " + day + ") ORDER BY TimeFrom ASC" + @" ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("BookingsAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

    }
}
