﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Bookings
    {
        public int BookingId { get; set; }
        public string UserName { get; set; }
        public string Room { get; set; }

        public DateTime TimeFrom { get; set; }

        public DateTime TimeTo { get; set; }

       
    }
}
