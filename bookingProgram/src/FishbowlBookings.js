import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import { Button, ButtonToolbar } from 'react-bootstrap';

export class FishbowlBookings extends Component {

    constructor(props) {
        super(props);
        this.state = { bookings: [] }
    }

    //get response for all fishbowl bookings
    refreshList() {
        fetch('https://localhost:5001/api/bookings/GetTheFishbowl')
            .then(response => response.json())
            .then(data => {
                this.setState({bookings:data});
            });
    }

    componentDidMount() {
        this.refreshList();
    }

    componentDidUpdate() {
        this.refreshList();
    }


    render() {
        const {bookings} = this.state;

        return (
            <div >
                <h4>The Fishbowl Bookings</h4>
                <Table className="mt-4" striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>BookingId</th>
                            <th>UserName</th>
                            <th>Room</th>
                            <th>TimeFrom</th>
                            <th>TimeTo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {bookings.map(booking =>
                            <tr key={booking.BookingId}>
                                <td>{booking.BookingId}</td>
                                <td>{booking.UserName}</td>
                                <td>{booking.Room}</td>
                                <td>{booking.TimeFrom.replace("T", " @ ")}</td>
                                <td>{booking.TimeTo.replace("T", " @ ")}</td>
                            </tr>)}
                    </tbody>
                </Table>
            </div>
        )
    }
}