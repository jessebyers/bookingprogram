import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

import { Button, ButtonToolbar } from 'react-bootstrap';
import { AddBookingModal } from './AddBookingModal';

export class AddBooking extends Component {

    constructor(props) {
        super(props);
        this.state = {addModalShow: false }
    }



    render() {
        
        let addModalClose = () => this.setState({ addModalShow: false });

        return (
            <div >
                {/* //just a button to add a booking */}
                <ButtonToolbar >
                    <Button variant='primary'
                        onClick={() => this.setState({ addModalShow: true })}>
                        Add Booking</Button>
                    <AddBookingModal show={this.state.addModalShow}
                        onHide={addModalClose} />
                </ButtonToolbar>
            </div>
        )
    }
}