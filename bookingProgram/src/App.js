import './App.css';
import { FishbowlBookings } from './FishbowlBookings';
import { BoardroomBookings } from './BoardroomBookings';

import { AddBooking } from './AddBooking';

function App() {
  return (
    <div className="App">

      <div className="container">
        <h3 className="m-3 d-flex justify-content-center">
          Booking System
        </h3>

        <body>
          <hr />

          {/* add booking button */}
          <div className="m-3 d-flex justify-content-center">
            <AddBooking />
          </div>
          <hr />
          {/* show all bookings */}
          <div className="m-3 d-flex justify-content-center">
          <BoardroomBookings />
          </div>
          <hr/>
          <div className="m-3 d-flex justify-content-center">
          <FishbowlBookings />
          </div>

        </body>
      </div>

    </div>
  );
}

export default App;
