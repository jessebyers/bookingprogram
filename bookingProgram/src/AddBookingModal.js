import React, { useEffect, Component, useState } from 'react';
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';
import moment from 'moment';
import { Table } from 'react-bootstrap';

import { FishbowlBookings } from './FishbowlBookings';
import { BoardroomBookings } from './BoardroomBookings';

import './App.css';

export class AddBookingModal extends Component {
    constructor(props) {
        super(props);
        this.state = { bookingsFishbowl: [] }
        this.state = { bookingsBoardroom: [] }

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = { roomValue: '' };
        this.state = { dateValue: '' };
        this.state = { timeTovalue: '' };
        this.state = { timeFromvalue: '' };
        this.state = { userNamevalue: '' };

        this.state = { bookingsBoardroomTimes: [] }

        this.handleRoomChange = this.handleRoomChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeToChange = this.handleTimeToChange.bind(this);
        this.handleTimeFromChange = this.handleTimeFromChange.bind(this);
        this.handleUserNameChange = this.handleUserNameChange.bind(this);
        this.handleReset = this.handleReset.bind(this);

        this.state = { bookings: [] }

    }



    handleSubmit(booking) {

        //validator 
        /*if (booking.target.Room.value = "The Boardroom") {
            //GET API
            fetch(process.env.REACT_APP_API + 'bookings/GetTheBoardroomTimes')
                .then(response => response.json())
                .then(data => {

                    var x;

                    for (x = 0; x < data.length; x++) {

                        //working
                        const From = new Date(data[x].TimeFrom);
                        const date = new Date(booking.target.Date.value + "T" + booking.target.TimeFrom.value);
                        const To = new Date(data[x].TimeTo);


                        //date check working
                        if (From <= date && To >= date) {
                            
                            x = data.length; //exit out of loop

                            alert("Date is in range")

                        } else {
                            alert("Date is not in range")

                            //bug when trying to run post method here
                        }
                    }
                    
                    //added switch here didnt work
                });
        }*/   
       //end of validator 


            //POST TO DATABASE
            fetch(process.env.REACT_APP_API + 'bookings', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    //BookingId: null,
                    UserName: booking.target.UserName.value,
                    Room: booking.target.Room.value,
                    TimeFrom: booking.target.Date.value + "T" + booking.target.TimeFrom.value,
                    TimeTo: booking.target.Date.value + "T" + booking.target.TimeTo.value,

                })
            })
                .then(res => res.json())
                .then((result) => {
                    alert(result);
                })

    }


    //Changes which room bookings table is shown depending on drop down select
    handleRoomChange(event) {

        this.setState({ roomValue: event.target.value });
        var option = event.target.value;

        if (option == 'The Boardroom') {
            document.getElementById('boardroomBookings').style.display = 'block';
            document.getElementById('fishbowlBookings').style.display = 'none';
            document.getElementById('date').style.display = 'block';

        } else if (option == 'The Fishbowl') {
            document.getElementById('fishbowlBookings').style.display = 'block';
            document.getElementById('boardroomBookings').style.display = 'none';
            document.getElementById('date').style.display = 'block';
        } else {
            document.getElementById('fishbowlBookings').style.display = 'none';
            document.getElementById('boardroomBookings').style.display = 'none';
            document.getElementById('date').style.display = 'none';

        }
    }

    //handles date changes. Displays next field
    handleDateChange(event) {

        this.setState({ dateValue: event.target.value });
        var option = event.target.value;

        if (option != '') {
            document.getElementById('timefrom').style.display = 'block';


        } else {
            document.getElementById('timefrom').style.display = 'none';

        }
    }

    //handles timefrom changes. Displays next field
    handleTimeFromChange(event) {

        document.getElementById('timeto').style.display = 'block';
    }

    //handles timeto changes. Displays next field
    handleTimeToChange(event) {

        document.getElementById('username').style.display = 'block';


    }

    //handles username changes. Displays next field
    handleUserNameChange(event) {

        this.setState({ userNameValue: event.target.value });
        var option = event.target.value;

        if (option == '') {
            document.getElementById('addbooking').style.display = 'none';

        } else {
            document.getElementById('addbooking').style.display = 'block';
        }

    }
    
    //resets fields - DOESNT RESET VALUES AT THIS TIME
    handleReset() {

        //hide all
        document.getElementById('date').style.display = 'none';
        document.getElementById('timefrom').style.display = 'none';
        document.getElementById('timeto').style.display = 'none';
        document.getElementById('username').style.display = 'none';
        document.getElementById('addbooking').style.display = 'none';
        document.getElementById('boardroomBookings').style.display = 'none';
        document.getElementById('fishbowlBookings').style.display = 'none';
    }



    render() {

        return (
            <div className="container">

                <Modal
                    {...this.props}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            Add Booking
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        <Row>
                            <Col sm={6}>
                                <Form onSubmit={this.handleSubmit}>

                                    <Form.Group controlId="Room">
                                        <Form.Label>Room</Form.Label>
                                        <Form.Select aria-label="Default select example" name="room" required onChange={this.handleRoomChange} value={this.state.value}  >
                                            <option selected="true" value="-">-</option>
                                            <option value="The Boardroom">The Boardroom</option>
                                            <option value="The Fishbowl">The Fishbowl</option>
                                        </Form.Select>
                                    </Form.Group>

                                    <div id="date">
                                        <Form.Group controlId="Date">
                                            <Form.Label>Date</Form.Label>
                                            <Form.Control type="date" name="Date" required
                                                placeholder="Date" onChange={this.handleDateChange} value={this.state.value} min={moment().format("YYYY-MM-DD")} />
                                        </Form.Group>
                                    </div>

                                    <div id="timefrom">
                                        <Form.Group controlId="TimeFrom">
                                            <Form.Label>TimeFrom</Form.Label>
                                            <Form.Control type="time" name="TimeFrom" required
                                                placeholder="TimeFrom" onChange={this.handleTimeFromChange} value={this.state.value} />
                                        </Form.Group>
                                    </div>

                                    <div id="timeto">
                                        <Form.Group controlId="TimeTo">
                                            <Form.Label>TimeTo</Form.Label>
                                            <Form.Control type="time" name="TimeTo" required
                                                placeholder="TimeTo" onChange={this.handleTimeToChange} value={this.state.value} />
                                        </Form.Group>
                                    </div>



                                    <div id="username">
                                        <Form.Group controlId="UserName">
                                            <Form.Label>UserName</Form.Label>
                                            <Form.Control type="text" name="UserName" required
                                                placeholder="UserName" onChange={this.handleUserNameChange} value={this.state.value} />
                                        </Form.Group>
                                    </div>

                                    <div id="addbooking">
                                        <Form.Group>
                                            <Button variant="primary" type="submit" onClick={this.props.onHide}>
                                                Add Booking
                                            </Button>
                                        </Form.Group>
                                    </div>

                                </Form>
                            </Col>
                        </Row>
                    </Modal.Body>


                    {/* Room bookings components/divs */}
                    <div id="boardroomBookings" >
                        <BoardroomBookings />
                    </div>

                    <div id="fishbowlBookings" >
                        <FishbowlBookings />
                    </div>


                    {/* reset button and close button */}
                    <Modal.Footer>
                        <Button variant="primary" type="reset" onClick={this.handleReset}>Reset</Button>
                        <Button variant="danger" onClick={this.props.onHide}>Close</Button>
                    </Modal.Footer>
                </Modal>




            </div>
        )
    }

}
