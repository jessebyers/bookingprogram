import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import { Button, ButtonToolbar } from 'react-bootstrap';

export class BoardroomBookings extends Component {

    constructor(props) {
        super(props);
        this.state = { bookings: [] }
    }

    //get resonse for all boardroom bookings
    refreshList() {
        fetch('https://localhost:5001/api/bookings/GetTheBoardroom')
            .then(response => response.json())
            .then(data => {
                this.setState({bookings:data});
            });
    }
    
    //mounts component once
    componentDidMount() {
        this.refreshList();
    }

    //updates when changes to state are made
    componentDidUpdate() {
        this.refreshList();
    }


    render() {
        const {bookings} = this.state;

        return (
            <div >
                <h4>The Boardroom Bookings</h4>

                {/* table code which maps response */}
                <Table className="mt-4" striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>BookingId</th>
                            <th>UserName</th>
                            <th>Room</th>
                            <th>TimeFrom</th>
                            <th>TimeTo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {bookings.map(booking =>
                            <tr key={booking.BookingId}>
                                <td>{booking.BookingId}</td>
                                <td>{booking.UserName}</td>
                                <td>{booking.Room}</td>
                                <td>{booking.TimeFrom.replace("T", " @ ")}</td>
                                <td>{booking.TimeTo.replace("T", " @ ")}</td>
                            </tr>)}
                    </tbody>
                </Table>
            </div>
        )
    }

}

